<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;


/**
 * Class RulePlanRange
 * @package App\Rules
 */
class RuleDailyLimit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        if(request()->type==ExchangeOrder::TYPE_BUY)  return true;



        /**
         * @var Wallet $mainWallet
         */
        $mainWallet = user()->wallets()->find(request()->main_wallet_id);


        if ($mainWallet->currency->code!='FST') return true;


        /**
         * @var Wallet $wallet
         */
        $wallet = user()->wallets()->find(request()->wallet_id);


        $orders_q = ExchangeOrder::active()
            ->whereType(ExchangeOrder::TYPE_BUY)
            ->where('main_currency_id', $mainWallet->currency_id)->where('currency_id', $wallet->currency_id)
            ->where('rate','>=', request()->rate)
            ->orderBy('rate', 'desc')
            ->orderBy('created_at', 'asc');

        $amount = $value*request()->rate*rate($wallet->currency->code, 'USD');

        if ($orders_q->exists())
        {

            $sum = $value;
            $amount = 0;
            $orders = $orders_q->get();

            /**
             * @var ExchangeOrder $order
             *
             */
            foreach ($orders as $order)
            {

                if ($sum<=0) break;

                if ($order->amount>=$sum)
                {
                    $amount = $amount+$sum*$order->rate*rate($wallet->currency->code, 'USD');
                    $sum = $sum-$order->amount;
                    break;
                }

                $sum = $sum-$order->amount;
                $amount = $amount+$order->amount*$order->rate*rate($wallet->currency->code, 'USD');

            }

            if ($sum>0)
            {
                $amount = $amount+$sum*request()->rate*rate($wallet->currency->code, 'USD');
            }
        }





        $limit = user()->dailyLimit();


        return $limit>$amount;



    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Exceeded daily limit');
    }
}
