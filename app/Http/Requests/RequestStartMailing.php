<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestStartMailing
 * @package App\Http\Requests
 *
 * @property string title
 * @property string send_to
 * @property string content
 */
class RequestStartMailing extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'send_to' => ['required'],
            'content' => ['required'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => __('Title is required'),
            'title.send_to' => __('Please, choose who will recipients'),
            'title.content' => __('Content is required'),
        ];
    }
}
