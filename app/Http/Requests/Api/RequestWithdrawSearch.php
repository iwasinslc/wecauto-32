<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestUserSearch
 * @package App\Http\Requests\Api
 * @property string login
 * @property string wallet_address
 * @property string batch_id
 */
class RequestWithdrawSearch extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'withdraw_param'    => 'required|string|max:191',
        ];
    }
}
