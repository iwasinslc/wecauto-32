<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestUserSearch
 * @package App\Http\Requests\Api
 * @property string login
 * @property string wallet_address
 * @property string batch_id
 */
class RequestUserSearch extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login'             => 'required_without_all:wallet_address,batch_id,customer_id,wallet_id,withdraw_param|max:150',
            'wallet_address'    => 'sometimes|required|string|max:191',
            'batch_id'          => 'sometimes|required', // can be string|integer
            'customer_id'       => 'sometimes|required|string|uuid',
            'wallet_id'         => 'sometimes|required|string|uuid',
            'withdraw_param'    => 'sometimes|required|string|max:191',
        ];
    }
}
