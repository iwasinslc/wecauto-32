<?php
namespace App\Http\Controllers\Telegram\account_bot\Settings\PersonalData;

use App\Http\Controllers\Controller;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Models\User;
use App\Modules\Messangers\TelegramModule;

class NameController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(
        TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event
    ) {
        sleep(1);
        $scope = TelegramBotScopes::where('command', '📣 Имя')
            ->where('bot_keyword', $bot->keyword)
            ->first();

        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }

//        if ($user->settingsChangesCount('name')==0) {
//            $this->haventChanges($webhook, $bot, $scope, $telegramUser, $event);
//            return response('ok');
//        }

        $message = view('telegram.account_bot.settings.name.index', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        /** @var User $user */
        $user       = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }

        $keyboard   = null;

//        if (password_verify('password', $user->password)) {
//            $keyboard = [
//                [
//                    ['text' => '⏩ заполнить профиль позже'],
//                ],
//            ];
//
//            $keyboard = [
//                'keyboard' => $keyboard,
//                'resize_keyboard' => true,
//            ];
//        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage(
                $event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                $keyboard,
                $scope,
                'keyboard'
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function checkAndProcessAnswer(
        TelegramWebhooks $webhook,
                                          TelegramBots $bot,
                                          TelegramBotScopes $scope,
                                          TelegramUsers $telegramUser,
                                          TelegramBotEvents $event,
                                          TelegramBotMessages $userMessage,
                                          TelegramBotMessages $botRequestMessage
    ) {
        /*
         * Validate inputs
         */
        $validator = \Validator::make([
            'name' => $userMessage->message
        ], [
            'name' => 'string|min:3|max:25',
        ]);

        if ($validator->fails()) {
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);
            return response('ok');
        }

        /*
         * Get and save info
         */
        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }

        $user->name = $userMessage->message;
        $user->save();

        /*
         * Answer
         */
        $this->userAnswerSuccess($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function userAnswerSuccess(
        TelegramWebhooks $webhook,
                                      TelegramBots $bot,
                                      TelegramBotScopes $scope,
                                      TelegramUsers $telegramUser,
                                      TelegramBotEvents $event,
                                      TelegramBotMessages $userMessage,
                                      TelegramBotMessages $botRequestMessage
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.settings.name.user_answer_success', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'userMessage'  => $userMessage,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        TelegramBotMessages::closeUserScopes($event, $bot);

        /*
         * Initiate: Set user sex
         */
        /** @var User $user */
        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }

        if ($user->email == $telegramUser->telegram_user_id) {
            app()->call(EmailController::class.'@index', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ]);
        }
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
                                     TelegramBots $bot,
                                     TelegramBotScopes $scope,
                                     TelegramUsers $telegramUser,
                                     TelegramBotEvents $event,
                                     TelegramBotMessages $userMessage,
                                     TelegramBotMessages $botRequestMessage
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.settings.name.user_answer_fails', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }



}
