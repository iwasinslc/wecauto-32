<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\News;
use App\Models\NewsLang;

class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = NewsLang::where('lang_id', Language::where('code', \App::getLocale())->first()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('customer.news.index', [
            'news' => $news
        ]);
    }


    public function item($slug)
    {

        $new = News::where('slug', $slug)->first();
        if (null===$new)
        {
            abort(404);
        }
        return view('customer.news.item', [
            'new' => $new
        ]);
    }
}
