<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCharity;
use App\Http\Requests\RequestCreateDeposit;
use App\Http\Requests\RequestReviews;
use App\Models\Deposit;
use App\Models\Language;
use App\Models\Rate;
use App\Models\Reviews;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReviewsController extends Controller
{

    public function index()
    {


        return view('profile.charity');
    }


    public function add()
    {


        return view('profile.add_review');
    }

    /**
     * @param RequestCreateDeposit $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(RequestReviews $request)
    {
        $review = new Reviews();
        $review->user_id = user()->id;
        $review->lang_id = Language::where('code', session('lang'))->first()->id;
        $review->name = $request->name;
        $review->type = $request->type;

        if ($request->type==0)
        {
            $review->text = $request->text;
        }
        else
        {
            $review->video = $request->video;
        }

        if ($review->save())
        {
            if ($request->type==1)
            {
                $img = $this->getYouTubeVideoId($request->video);
                if ($img!==null)
                {
                    $review->image = $img;
                    $review->save();
                }
            }
            return back()->with('success', __('Review has been added'));
        }
        return back()->with('error', __('Unable to update review'))->withInput();
    }


    function getYouTubeVideoId($pageVideUrl) {
        $link = $pageVideUrl;
        $video_id = explode("?v=", $link);
        if (!isset($video_id[1])) {
            $video_id = explode("youtu.be/", $link);
        }


        if (empty($video_id[1])) $video_id = explode("/v/", $link);
        $video_id = explode("&", $video_id[1]);
        $youtubeVideoID = $video_id[0];
        if ($youtubeVideoID) {
            return 'https://img.youtube.com/vi/' . $youtubeVideoID. '/maxresdefault.jpg';
        } else {
            return null;
        }
    }


}
