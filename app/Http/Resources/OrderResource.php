<?php

namespace App\Http\Resources;

use App\Models\ExchangeOrder;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="OrderResource",
 *     description="Order resource",
 *     type="object",
 *     @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Order"))
 * )
 */
class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ExchangeOrder $this */
        return [
            'id' => $this->id,
            'type' => $this->type,
            'value' => $this->amount,
            'currency' => $this->mainCurrency->code,
            'rate' => $this->rate,
            'rate_currency' => $this->currency->code,
            'rate_amount' => $this->amount * $this->rate,
            'licence_rate' => $this->licence_rate,
            'licence_currency' => 'USD',
            'status' => $this->active ? 'active' : 'not active',
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
