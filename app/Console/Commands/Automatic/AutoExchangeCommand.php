<?php
namespace App\Console\Commands\Automatic;

use App\Events\ExchangeAllOperations;
use App\Events\NotificationEvent;
use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\DepositQueue;
use App\Models\ExchangeOrder;
use App\Models\OrderRequest;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Console\Command;

/**
 * Class AutoExchangeCommand
 * @package App\Console\Commands\Automatic
 */
class AutoExchangeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:exchange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposits queues.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        while (true) {

            $requests = OrderRequest::where('active', 1)->where('available_at', '<=', now()->toDateTimeString())->orderBy('created_at')->get();

            foreach ($requests as $request) {
                /**
                 * @var OrderRequest $request
                 */


                $rate = $request->rate;

                $type = $request->type;

                $amount = $request->amount;

                $orders_q = ExchangeOrder::active()
                    ->whereType($request->type==ExchangeOrder::TYPE_BUY ? ExchangeOrder::TYPE_SELL : ExchangeOrder::TYPE_BUY)
                    ->where('main_currency_id', $request->main_currency_id)->where('currency_id', $request->currency_id);


                if ($request->type==ExchangeOrder::TYPE_BUY)
                {
                    $orders_q = $orders_q->where('rate','<=', $rate);
                    $orders_q = $orders_q->orderBy('rate', 'asc');
                    $orders_q = $orders_q->orderBy('created_at', 'asc');
                }
                else
                {
                    $orders_q = $orders_q->where('rate','>=', $rate);
                    $orders_q = $orders_q->orderBy('rate', 'desc');
                    $orders_q = $orders_q->orderBy('created_at', 'asc');
                }


                $data = [
                    'user'=>$request->user,
                    'main_wallet_id'=>$request->main_wallet_id,
                    'wallet_id'=>$request->wallet_id,
                    'amount'=>$request->amount,
                    'type'=>$request->type,
                    'rate'=>$request->rate
                ];

                if ($orders_q->count()==0)
                {

                    ExchangeOrder::addOrder($data);

                    $request->updateRemoveAmount($amount, $type, $rate);

                    continue;

                }


                $orders = $orders_q->get();


                foreach ($orders as $order)
                {
                    /**
                     * @var ExchangeOrder $order
                     */
                    if ($amount<=0)
                    {
                        $this->info('0-assigned');
                        break;
                    }

//                    if ($type==ExchangeOrder::TYPE_SELL)

                    $this->info('assigned '.$amount);


                    $pay = $amount>$order->amount ? $order->amount : $amount;

                    $request->updateRemoveAmount($pay, $type, $order->rate);

                    $order_amount = $order->amount;
                    $order->exchange($request->wallet, $request->mainWallet, $amount);
                    $amount = $amount - $order_amount;


                }


                if ($amount>0)
                {




                    $data['amount'] = $amount;
                    ExchangeOrder::addOrder($data);


                    $request->updateRemoveAmount($amount, $type, $rate);
                }



                if ($request->remove_amount>0&&$type==ExchangeOrder::TYPE_BUY)
                {
                    $user_wallet =  $request->wallet;

                    $user_wallet->addAmountWithoutAccrueToPartner($request->remove_amount);
                }



                $request->active = 0;
                $request->save();



            }
            sleep(1);
        }
    }
}
