<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderRequest
 * @package App\Models
 *
 * @property integer OrderRequest
 * @property string currency_id
 * @property string main_currency_id
 * @property string wallet_id
 * @property string main_wallet_id
 * @property Wallet mainWallet
 * @property Wallet wallet
 * @property integer type
 * @property string user_id
 * @property float amount
 * @property float rate
 * @property integer active
 * @property float free_amount
 * @property float remove_amount
 * @property Carbon available_at
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class OrderRequest extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'currency_id',
        'main_currency_id',
        'wallet_id',
        'main_wallet_id',
        'type',
        'user_id',
        'amount',
        'closed',
        'active',
        'rate',
        'free_amount',
        'available_at',
        'remove_amount'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mainCurrency()
    {
        return $this->belongsTo(Currency::class, 'main_currency_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mainWallet()
    {
        return $this->belongsTo(Wallet::class, 'main_wallet_id');
    }


    /**
     * @param Wallet $main_wallet
     * @param $mainCurrency
     * @param Wallet $wallet
     * @param $currency
     * @param float $amount
     * @param int $type
     * @param int $rate
     * @return OrderRequest
     * @throws \Exception
     */
    public static function addRequest(Wallet $main_wallet, Currency $mainCurrency, Wallet $wallet, Currency $currency, float $amount, $type=0, $rate=0)
    {
        /** @var User $user */

        $user = $main_wallet->user;

        try {
            $order = new OrderRequest();
            $order->user_id = $user->id;
            $order->main_currency_id = $main_wallet->currency_id;
            $order->currency_id = $wallet->currency_id;
            $order->main_wallet_id = $main_wallet->id;
            $order->wallet_id = $wallet->id;
            $order->active = 1;
            $order->rate = $rate;
            $order->type = $type;
            $order->amount = $amount;
            $order->free_amount = $amount;
            $order->available_at = now()->toDateTimeString();

            $save = $order->save();

//            \DB::commit();
        } catch (\Exception $e) {
//            \DB::rollback();
            throw new \Exception("Request Error");
        }


        if ($save!==null)
        {

            $user_wallet = $type==ExchangeOrder::TYPE_SELL ?  $main_wallet : $wallet;

            $remove = $type==ExchangeOrder::TYPE_SELL ?  $amount : $amount*$rate;

            $user_wallet->removeAmount($remove);

            if ($mainCurrency->code == 'FST') {
                if ($order->type == ExchangeOrder::TYPE_SELL) {
                    //PriceUps::priceDown($amount);
                    $user->removeLimitsSell($amount, $mainCurrency->code,$rate*rate($currency->code,'USD'));

                } else {
                    $user->removeLimitsBuy($amount, $mainCurrency->code, $rate*rate($currency->code, 'USD'));
                }
            }
            else
            {
                if ($order->type == ExchangeOrder::TYPE_SELL) {
                    //PriceUps::priceDown($amount);
                    $user->removeDepositSellLimit($amount*$rate);

                }
            }


            $order->remove_amount = $remove;
            $order->save();
        }


        return $order;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }


    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }


    public function scopeBetween($query, Carbon $from, Carbon $to)
    {
        $query->whereBetween('order_requests.created_at', [$from, $to]);
    }


    public function updateRemoveAmount($amount, $type, $rate)
    {
        $remove = $type==ExchangeOrder::TYPE_SELL ?  $amount : $amount*$rate;

        $this->remove_amount = $this->remove_amount-$remove;
        $this->active = 0;
        $this->save();
    }


}
