<?php
namespace App\Models;

use App\Traits\ModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Faq
 * @package App\Models
 *
 * @property string lang_id
 * @property string title
 * @property string text
 * @property Carbon created_at
 */
class Faq extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'lang_id',
        'title',
        'text',
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang()
    {
        return $this->belongsTo(Language::class, 'lang_id');
    }

}
