<?php
namespace App\Models;

use App\Jobs\HandleRankJob;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Deposit
 * @package App\Models
 *
 * @property string id
 * @property User user
 * @property string currency_id валюта
 * @property string wallet_id кошелек с которого создается депозит, на него же возвращается при закрытии
 * @property string user_id
 * @property string rate_id ИД тарифного плана
 * @property float daily процент ежедневных начислений
 * @property int duration продолжительность действия депозита (в днях) равно кол-ву ежедневных начислений
 * @property float payout выплата начальной суммы в процентах
 * @property float invested начальная сумма депозита
 * @property float balance текущий баланс (с учетом начислений)
 * @property int reinvest ставка реинвестирования
 * @property int autoclose закрываем депозит по графику
 * @property int active статус
 * @property int type
 * @property integer min_days
 * @property integer start_percent
 * @property integer max_fst
 * @property float fst_in_usd
 * @property int fast_days
 * @property string name
 * @property string image
 * @property string condition последнее действие
 * @property Carbon datetime_closing
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Deposit extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'currency_id',
        'user_id',
        'wallet_id',
        'type',
        'fast_days',
        'name',
        'image',
        'rate_id',
        'daily',
        'duration',
        'payout',
        'invested',
        'balance',
        'reinvest',
        'autoclose',
        'active',
        'condition',
        'datetime_closing',
        'created_at',
        'fst_in_usd',
        'min_days',
        'start_percent',
        'max_fst'
    ];


    protected $dates = [
        'datetime_closing'
    ];

//    protected $casts = [
//        'datetime_closing' => 'timestamp'
//    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'deposit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function queues()
    {
        return $this->hasMany(DepositQueue::class, 'deposit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rate()
    {
        return $this->belongsTo(Rate::class, 'rate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    /**
     * @return mixed
     */
    public function paymentSystem()
    {

        if ($this->wallet!==null) return $this->wallet->paymentSystem();

        return null;

    }


    /**
     * @param $value
     * @return float
     * @throws \Exception
     */
    public function getBalanceAttribute($value)
    {
        if (isset($this->currency->code)) {
            return currencyPrecision($this->currency->id, $value);
        };
        return $value;
    }

    /**
     * @param $value
     * @return float
     * @throws \Exception
     */
    public function getInvestedAttribute($value)
    {
        if (isset($this->currency->code)) {
            return currencyPrecision($this->currency->id, $value);
        };
        return $value;
    }


    public function addFst($days)
    {
        $this->duration = $this->duration-$days;
        $this->datetime_closing = $this->datetime_closing->subDays($days);
        $this->fast_days = $this->fast_days+$days;
        $this->fst_in_usd =  $this->fst_in_usd+($days*$this->dayFst());

        $amount = $days*$this->dayFst(true);

        $wallet = $this->user->getUserWallet('FST');

        $transaction = $this->save()
            ? Transaction::addFst($wallet, $amount, $this)
            : null;

        if (null != $transaction && $wallet->removeAmount($amount)) {
            $transaction->update(['approved' => true]);

            return $this
                ?
                : null;
        };

    }

    /**
     * @param $field
     * @return Deposit|null
     * @throws \Exception
     */
    public static function addDeposit($field)
    {
        /** @var User $user */
        $user     = isset($field['user']) ? $field['user'] : Auth::user();
        /** @var Wallet $wallet */
        $wallet   = $user->wallets()->where('id', $field['wallet_id'])->first();
        $amount   = abs($field['amount']);

        $name   = htmlspecialchars($field['name']);
        $image   =  isset($field['image']) ? htmlspecialchars($field['image']) : '';
        $invested   = abs($field['invested']);
        $type   = abs($field['type']);
        $reinvest = array_key_exists('reinvest', $field) ? abs($field['reinvest']) : 0;
        /** @var Rate $rate */
        $rate     = Rate::find($field['rate_id']);

        $deposit                    = new Deposit;
        $deposit->name           = $name;
        $deposit->image           = $image;
        $deposit->rate_id           = $rate->id;
        $deposit->currency_id       = $rate->currency_id;
        $deposit->wallet_id         = $wallet->id;
        $deposit->user_id           = $user->id;
        $deposit->invested          = $invested;
        $deposit->daily             = $rate->daily;
        $deposit->duration          = abs(now()->addMonths(12)->diffInDays(now()));
        $deposit->payout            = $rate->payout;
        $deposit->balance           = $amount;
        $deposit->reinvest          = $reinvest;
        $deposit->autoclose         = $rate->autoclose;
        $deposit->condition         = 'create';
        $deposit->min_days = self::minDays();
        $deposit->start_percent = self::startPercent();
        $deposit->max_fst = self::maxFst();
        $deposit->type = $type;
        $deposit->datetime_closing  = now()->addMonths(12);
        $deposit->created_at        = isset($field['created_at']) ? $field['created_at'] : now();

        $transaction = $deposit->save()
            ? Transaction::createDeposit($deposit)
            : null;

        if (null != $transaction && $deposit->wallet->removeAmount($amount)) {
            $transaction->update(['approved' => true]);
            $deposit->update(['active' => true]);

            $user->removeDepositSellLimit($amount*rate('WEC', 'USD'));

            if ($deposit->max_fst==20)
            {
                $buy_limit = $user->licence->buy_amount;
                $max_upgrade = $invested*$deposit->max_fst*0.01;
                if ($max_upgrade>$buy_limit)
                {
                    $user->buy_limit = $user->buy_limit+($max_upgrade-$buy_limit);
                    $user->save();
                }

            }

            // send notification to user
            $data = [
                'deposit' => $deposit
            ];
            $user->sendNotification('deposit_opened', $data);

            self::priceUp($invested*self::startPercent()*0.01);


            $user->partner->checkRank(true);

            //HandleRankJob::dispatch($user->partner)->onQueue(getSupervisorName() . '-high')->delay(now());


            return $deposit
                ?
                : null;
        };
        throw new \Exception("Transaction start or wallet error! ".print_r($field,true));
    }




    public static function startPercent()
    {
        $day = now()->dayOfWeekIso;
        $percent = 35;
        if ($day!=6) return $percent;

        if (user()->deposits()->count()>0) return $percent;

        if (user()->transactions()
                ->where('type_id', TransactionType::getByName('convert')->id)->count()>0

        ) return $percent;

        if (user()->transactions()
            ->where('type_id', TransactionType::getByName('transfer_receive')->id)->count()>0
        ) return $percent;

        if (user()->orderPieces()->where('main_currency_id', Currency::getByCode('WEC')->id)->where('currency_id', Currency::getByCode('USD')->id)->where('type', 1)->sum('amount')<1)return $percent;

        return 30;
    }


    public static function minDays()
    {


        $day = now()->dayOfWeekIso;
        $days = 120;
        if ($day!=6) return $days;

        if (user()->deposits()->count()>0) return $days;

        if (user()->transactions()
                ->where('type_id', TransactionType::getByName('convert')->id)->count()>0

        ) return $days;

        if (user()->transactions()
                ->where('type_id', TransactionType::getByName('transfer_receive')->id)->count()>0

        ) return $days;

        if (user()->orderPieces()->where('main_currency_id', Currency::getByCode('WEC')->id)->where('currency_id', Currency::getByCode('USD')->id)->where('type', 1)->sum('amount')<1) return $days;

        return 90;
    }

    public static function maxFst()
    {
        $day = now()->dayOfWeekIso;
        $max = 15;
        if ($day!=6) return $max;

        if (user()->deposits()->count()>0) return $max;

        if (user()->transactions()
                ->where('type_id', TransactionType::getByName('convert')->id)->count()>0

        ) return $max;

        if (user()->transactions()
                ->where('type_id', TransactionType::getByName('transfer_receive')->id)
        ) return $max;

        if (user()->orderPieces()->where('main_currency_id', Currency::getByCode('WEC')->id)->where('currency_id', Currency::getByCode('USD')->id)->where('type', 1)->sum('amount')<1) return $max;

        return 20;
    }



    /**
     * @return bool
     */


    public function dayFst($in_fst = false)
    {

        $days = Carbon::parse($this->created_at)->addDays(365)->diffInDays(Carbon::parse($this->created_at)->addDays($this->min_days));
        $amount = $this->invested*$this->max_fst*0.01;

        if ($in_fst)
        {
            $amount = $amount*rate('USD', 'FST');
        }

        return $amount/$days;
    }

    public function progressPercentage($add_days = 0) {
        $days = $this->created_at->addDays(365)->diffInDays($this->created_at);
        $passed = $this->created_at->diffInDays($this->datetime_closing);
        return 100 * ($days - $passed + $add_days) / $days;
    }


    /**
     * @return int
     */
    public function minNumberDays()
    {
        return $this->min_days;
    }

    /**
     * Allowed days
     * @return int
     */
    public function allowedExecutionDays() {
        return $this->maxNumberDays() - $this->minNumberDays();
    }

    /**
     * Get max day
     * @return int
     */
    public function maxNumberDays() {



        $days = $this->created_at->addDays(365)->diffInDays($this->created_at);
        $passed_days = now()->diffInDays($this->created_at);

        $needed = $days - $this->fast_days - $passed_days;
        return $needed;
    }


    /**
     * @param DepositQueue $depositQueue
     * @return bool
     * @throws \Throwable
     */
    public function close()
    {

        if (!$this->active) {
            throw new \Exception("failed close");
        }

        /** @var Wallet $wallet */
        $wallet = $this->wallet()->first();

        /** @var User $user */
        $user = $this->user()->first();

        $amount = $this->invested * rate('USD', 'WEC');
        $closeTransaction = Transaction::closeDeposit($this, $amount);



        if ($amount > 0 && !$wallet->addAmountWithoutAccrueToPartner($amount)) {
            throw new \Exception("deposit not close!");
        }

        $closeTransaction->update(['approved' => true]);
        $this->update(['condition' => 'closed']);
        $this->update(['active' => false]);

        $user->addDepositSellLimit($this->invested);

        // send notification to user
        try {
            $data = [
                'deposit' => $this
            ];
            $user->sendNotification('deposit_closed', $data);
        } catch(\Exception $e) {
            return true;
        }
        return true;
    }

    /**
     * @param float $amount
     * @return bool
     */
    public function addBalance($amount=0.00)
    {
        return $this->update(['balance' => $this->balance + $amount]);
    }

    /**
     * @return mixed
     */
    public function investTransaction()
    {
        $typeId = TransactionType::getByName('create_dep')->id;

        return Transaction::where([
            'wallet_id' => $this->wallet_id,
            'type_id' => $typeId,
            'deposit_id' => $this->id,
        ])->orderBy('created_at')->first();
    }

    /**
     * @return bool
     */
    public function block()
    {
        if ($this->active != true || $this->condition == 'blocked' || $this->condition == 'closed') {
            return false;
        }

        $this->condition = 'blocked';
        $this->active = false;

        $this->save();
        return true;
    }

    /**
     * @return bool
     */
    public function unblock()
    {
        if ($this->active != false || $this->condition != 'blocked') {
            return false;
        }

        $this->active    = true;
        $this->condition = 'onwork';

        $this->save();
        return true;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function closedBalances(): array
    {
        $deposits = Currency::join('deposits', 'currencies.id', '=', 'deposits.currency_id')
            ->where('deposits.condition', 'closed')
            ->select('currencies.code', 'deposits.balance')
            ->get();

        $balances = Currency::balances();

        foreach ($deposits as $item) {
            $balances[$item->code] = key_exists($item->code, $balances)
                ? $balances[$item->code] + $item->balance
                : $item->balance;
        }
        return $balances;
    }


    public static function priceUp($amount)
    {
        $price = rate('FST', 'USD');

        $end_amount = 10000000;

        $current_amount = (float)Setting::getValue('price_up_amount');


        $summary_amount = $current_amount+$amount;



        $base_price = (float)Setting::getValue('base_price');


        if ($base_price<=0)  $base_price = $price;

        $percent = $summary_amount/$end_amount;


        if ($summary_amount>=$end_amount)
        {
            $base_price = $base_price*2;

            $summary_amount = $summary_amount-$end_amount;

            $percent = $summary_amount/$end_amount;


        }


        $price = $base_price + $base_price*$percent;


        Setting::setValue('fst_to_usd', $price);

        Setting::setValue('price_up_amount', $summary_amount);

        Setting::setValue('base_price', $base_price);

    }
}
