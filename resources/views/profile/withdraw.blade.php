@extends('layouts.profile')
@section('title', __('Withdraw'))
@section('content')
    <section class="section-tabs">
        <div class="container">
            @include('partials.profile.finance_menu')
            <div class="section-tabs__content">
                <form class="tabs-form" method="POST" action="{{ route('profile.withdraw') }}">
                    {{ csrf_field() }}


                    <p class="tabs-form__subtext">{{ __('Commission').' USD, USDT, ETH, PZM - 5%,
                    WEC - 1 %'}}, {{__('Minimum withdraw')}} USD - 10 USD, в  USDT - 20 USDT, ETH -  100 USD, WEC - 1


{{--                        {{__('Limit')}} {{number_format(user()->sellLimit(), 2)}} USD--}}
{{--                        ({{number_format(user()->sellLimit()*rate('USD', 'FST'), 2)}} ACC)--}}
                    </p>

                    <div class="tabs-form__row">
                        <div class="tabs-form__col-12">
                            <div class="field">
                                <label>{{__('Amount')}}</label>
                                <input id="topup_amount_modal" type="number" step="any" class="js-input-number" value="0.1" name="amount" required>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-form__row">
                        <div class="tabs-form__col-6">
                            <div class="field">
                                <label>{{__('System')}}</label>
                                <label class="select">
                                    <select id="withdraw_ps_list" name="currency">
                                        @foreach ($wec_ps as $payment_system)
                                            @foreach ($payment_system->currencies as $currency)
                                                {{--                                        @if ($currency->code!=='USD')--}}

                                                <option data-currency="WEC"
                                                        value="{{$currency->id}}:{{$payment_system->id}}">({{__('In')}}
                                                    WEC) - {{$payment_system->name}} {{$currency->code}}</option>


                                            @endforeach
                                        @endforeach

                                        @foreach ($usd_ps as $payment_system)
                                            @foreach ($payment_system->currencies as $currency)
                                                @if ($currency->code!=='BTC'&&$currency->code!=='ETH'&&$currency->code!=='USD')
                                                    @if ($payment_system->code!=='free-kassa'&&$payment_system->code!=='payeer')
                                                        <option data-currency="USD"
                                                                value="{{$currency->id}}:{{$payment_system->id}}">
                                                            ({{__('In')}} USD)
                                                            - {{$payment_system->name}} {{$currency->code}}</option>
                                                    @endif
                                                @endif


                                            @endforeach
                                        @endforeach

{{--                                            @foreach ($pzm_ps as $payment_system)--}}
{{--                                                @foreach ($payment_system->currencies as $currency)--}}

{{--                                                    <option data-currency="PZM"--}}
{{--                                                            value="{{$currency->id}}:{{$payment_system->id}}">({{__('In')}}--}}
{{--                                                        PZM) - {{$payment_system->name}} {{$currency->code}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            @endforeach--}}


{{--                                        --}}{{--                                        @if (user()->hasRole('root'))--}}

{{--                                        @foreach ($pzm_ps as $payment_system)--}}
{{--                                            @foreach ($payment_system->currencies as $currency)--}}

{{--                                                <option data-currency="PZM"--}}
{{--                                                        value="{{$currency->id}}:{{$payment_system->id}}">({{__('In')}}--}}
{{--                                                    PZM) - {{$payment_system->name}} {{$currency->code}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        @endforeach--}}
{{--                                        --}}{{--                                        @endif--}}


{{--                                        @if (user()->hasRole('root'))--}}

{{--                                            @foreach ($bip_ps as $payment_system)--}}
{{--                                                @foreach ($payment_system->currencies as $currency)--}}

{{--                                                    <option data-currency="BIP"--}}
{{--                                                            value="{{$currency->id}}:{{$payment_system->id}}">--}}
{{--                                                        ({{__('In')}} BIP)--}}
{{--                                                        - {{$payment_system->name}} {{$currency->code}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}




                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="tabs-form__col-6">
                            <div class="field">
                                <label>{{__('Address')}}</label>
                                <input type="text" value="" name="external" required>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-form__row" id="comment" style="display: none;">
                        <div class="tabs-form__col-12">
                            <div class="field">
                                <label>{{__('Comment')}}</label>
                                <input type="number" step="1" value="" name="comment">
                            </div>
                        </div>
                    </div>
                    <div class="tabs-form__bottom">
                        <button class="btn btn--warning btn--size-md">{{__('Withdraw')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>


    @include('partials.profile.withdraw_filter')
    <!-- /.card -->
@endsection
@push('load-scripts')
    <script>


        $('body').on('change', '#withdraw_ps_list', function () {
            code = $(this).find('option:selected').data('currency');

            if (code == 'PZM') {
                $('#comment').show();
            } else {
                $('#comment').hide();
            }

        });
    </script>
@endpush