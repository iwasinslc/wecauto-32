@extends('admin/layouts.app')
@section('title', __('Cashback request'))
@section('breadcrumbs')
    <li> {{ __('Cashback request') }}</li>
@endsection

@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">@yield('title')</h1>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Name:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="{{ route('admin.users.index', ['user' => $user->id]) }}">
                                        {{ $user->name }}
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('E-mail:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="mailto:{{ $user->email }}" target="_blank">{{ $user->email }}</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Telegram:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $cashbackRequest->telegram }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Documents:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    @foreach($documents as $document)
                                        <span>
                                            <a href="{{app('env') === 'develop' ? $document->getFullUrl() : $document->getUrl()}}" download>{{$document->name}}</a>
                                        </span>
                                    @endforeach
                                    @if(!$cashbackRequest->documents_checked)
                                    <form style="float:right;" action="{{route('admin.cashback.update', ['cashback' => $cashbackRequest->id])}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <input type="text" hidden name="documents_checked" value="1">
                                        <button class="btn btn-xs btn-primary">Verify document</button>
                                    </form>
                                    @else
                                        <span class="green btn-success">(Verified)</span>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Video link:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    <a href="{{$cashbackRequest->video_link}}" target="_blank">
                                        {{ $cashbackRequest->video_link }}
                                    </a>
                                    @if(!$cashbackRequest->video_checked)
                                    <form style="float:right;" action="{{route('admin.cashback.update', ['cashback' => $cashbackRequest->id])}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <input type="text" hidden name="video_checked" value="1">
                                        <button class="btn btn-xs btn-primary">Verify video</button>
                                    </form>
                                    @else
                                        <span class="green btn-success">(Verified)</span>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-5">
                                    {{ __('Comment:') }}
                                </div>
                                <div class="col-lg-7" style="font-weight:bold;">
                                    {{ $cashbackRequest->comment }}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">Result</h1>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    @if($cashbackRequest->approved === null)
                    <form class="form-horizontal" method="POST" action="{{ route('admin.cashback.update', ['cashback' => $cashbackRequest->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="result" class="col-md-4 control-label">{{ __('Result') }}</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="result" required>{{ old('result', $cashbackRequest->result) }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="approved">Approved</label>
                            <div class="col-md-6">
                                <div>
                                    <input type="radio" name="approved" value="1" {{$cashbackRequest->result === true ? 'checked' : ''}}> Yes
                                </div>
                                <div>
                                    <input type="radio" name="approved" value="0" {{$cashbackRequest->result === false ? 'checked' : ''}}> No
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save result') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @else
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-5">
                                        {{ __('Status') }}
                                    </div>
                                    <div class="col-lg-7" style="font-weight:bold;">
                                        {{$cashbackRequest->approved ? "Approved" : "Rejected"}}
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-5">
                                        {{ __('Result') }}
                                    </div>
                                    <div class="col-lg-7" style="font-weight:bold;">
                                        {{$cashbackRequest->result}}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    @endif
                </div>
            </section>
        </div>
    </div>
@endsection